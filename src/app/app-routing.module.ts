import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {InitializeGameComponent} from "./component/initialize-game/initialize-game.component";
import {PlayGameComponent} from "./component/play-game/play-game.component";

const routes: Routes = [
  {path: '', pathMatch: 'full', component: InitializeGameComponent},
  {path: 'play-game/:gameid', pathMatch: 'full', component: PlayGameComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
