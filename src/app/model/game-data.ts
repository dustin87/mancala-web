import {PlayerDto} from "./player-dto";
import {Pit} from "./pit";

export class GameData {

  public id: string;
  public players: PlayerDto[];
  public pits: Pit[];
  public lastPlayer: string;
  public nextPlayer: string;
  public lastUpdatedPitIndex: number;
  public isCompleted: boolean;
  public winner: string;
  
}
