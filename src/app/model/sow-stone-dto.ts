export class SowStoneDto {
  public gameId: string;
  public pitIndex: number;
  public playedBy: string;
}
