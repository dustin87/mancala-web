import {PlayerDto} from "./player-dto";

export class InitializeGameDto {
  public players: PlayerDto[];
}
