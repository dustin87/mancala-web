import { Component, OnInit } from '@angular/core';
import {PlayerDto} from "../../model/player-dto";
import {GeneralUtil} from "../../util/general-util";
import {InitializeGameDto} from "../../model/initialize-game-dto";
import {GameService} from "../../service/game.service";
import {HttpErrorResponse} from "@angular/common/http";
import {GameData} from "../../model/game-data";
import {Router} from "@angular/router";

@Component({
  selector: 'app-initialize-game',
  templateUrl: './initialize-game.component.html',
  styleUrls: ['./initialize-game.component.css']
})
export class InitializeGameComponent implements OnInit {

  public playerA = new PlayerDto();
  public playerB = new PlayerDto();

  constructor(public gameService: GameService,
              public router: Router) { }

  ngOnInit(): void {

    var a;

    a +=1;

    console.log(a);

  }

  public initialize() {
    if (!GeneralUtil.isValidString(this.playerA.fullName)) {
      alert('Please provide a valid name for Player A');
      return;
    }

    if (!GeneralUtil.isValidString(this.playerB.fullName)) {
      alert('Please provide a valid name for Player B');
      return;
    }

    this.playerA.category = 'PLAYER_A';
    this.playerB.category = 'PLAYER_B';

    let playerArray: PlayerDto[] = [];
    playerArray.push(this.playerA);
    playerArray.push(this.playerB);

    let initiatlizeGameDto = new InitializeGameDto();
    initiatlizeGameDto.players = playerArray;

    this.gameService.initializeGame(initiatlizeGameDto).subscribe(
      data => {
        let gameData: GameData = data;
        console.log(gameData);

        this.router.navigate(['/play-game/' + gameData.id]);
      },
      error => {
        this.errorHandler(error);
      }
    )
  }



  public errorHandler(error: any) {

    if (error instanceof HttpErrorResponse) {

      if (error.error.message) {
        alert(error.error.message);
        return;
      }

      alert('Network error');
      return;
    }
    alert('Network error');
    return;
  }

}
