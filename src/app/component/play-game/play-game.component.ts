import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {GameService} from "../../service/game.service";
import {GameData} from "../../model/game-data";
import {HttpErrorResponse} from "@angular/common/http";
import {SowStoneDto} from "../../model/sow-stone-dto";

@Component({
  selector: 'app-play-game',
  templateUrl: './play-game.component.html',
  styleUrls: ['./play-game.component.css']
})
export class PlayGameComponent implements OnInit {

  public gameData: GameData;


  constructor(public route: ActivatedRoute,
              public router: Router,
              public gameService: GameService) { }

  ngOnInit(): void {
    this.getGameData();
  }

  getGameData() {
    let gameid: string | null;

    this.route.paramMap.subscribe((params: ParamMap)=>{
      gameid  = params.get('gameid');
    });


    this.gameService.getGameData(gameid).subscribe(
      data => {
          this.gameData = data;
      },
      error => {
        console.log("error", error);
        this.errorHandler(error);
      }
    )
  }


  playGame(pitIndex: number) {

    let dto = new SowStoneDto();
    dto.gameId = this.gameData.id;
    dto.pitIndex = pitIndex;
    dto.playedBy = this.gameData.nextPlayer;

    this.gameService.sowStone(dto).subscribe(
      data => {
        this.gameData = data;
      },
      error =>  {
        this.errorHandler(error);
      }
    )
  }

  public removeUnderscore(text: string): string {
    return text.replace('_', ' ');
  }


  public errorHandler(error: any) {

    if (error instanceof HttpErrorResponse) {

      if (error.error.message) {
        alert(error.error.message);

        if (error.status == 404) {
          this.router.navigate(['/']);
          return;
        }
        return;
      }

      alert('Network error');
      return;
    }
    alert('Network error');
    return;
  }

}
