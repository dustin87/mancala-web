import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {InitializeGameDto} from "../model/initialize-game-dto";
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {environment} from "../../environments/environment";
import {GameData} from "../model/game-data";
import {SowStoneDto} from "../model/sow-stone-dto";

@Injectable({
  providedIn: 'root'
})
export class GameService {

  constructor(public httpClient: HttpClient) { }

  public initializeGame(dto: InitializeGameDto): Observable<GameData> {
    const url = environment.baseUrl + '/api/game';

    return this.httpClient.post<GameData>(url, dto)
      .pipe(catchError(this.errorHandler));
  }

  public getGameData(gameid: any): Observable<GameData> {
    const url = environment.baseUrl + '/api/game/' + gameid;

    return this.httpClient.get<GameData>(url)
      .pipe(catchError(this.errorHandler));
  }

  public sowStone(dto: SowStoneDto): Observable<GameData> {
    const url = environment.baseUrl + '/api/game/sow-stones';

    return this.httpClient.patch<GameData>(url, dto)
      .pipe(catchError(this.errorHandler));
  }

  // Error Handler
  errorHandler(error: HttpErrorResponse) {
    return throwError(error);
  }
}
